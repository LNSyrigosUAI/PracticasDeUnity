﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    private float VerticalInput;
    private float HorizontalInput;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Obtener la entrada vertical del usuario
        VerticalInput = Input.GetAxis("Vertical");

        // Mueva el avión hacia adelante a una velocidad constante.
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        // Inclina el avión hacia arriba/abajo según las teclas de flecha arriba/abajo.
        transform.Rotate(Vector3.right, rotationSpeed * Time.deltaTime * VerticalInput);
    }
}
