using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Control : MonoBehaviour
{
    //Variables privadas.
    private float Velocidad = 10.0f;
    private float VelocidadGiro = 25.0f;
    private float InputHorizontal;
    private float InputAdelante;

    void Start()
    {
        
    }

    void Update()
    {
        InputHorizontal = Input.GetAxis("Horizontal");
        InputAdelante = Input.GetAxis("Vertical");

        //Mover el vehiculo hacia adelante.
        transform.Translate(Vector3.forward * Time.deltaTime * Velocidad * InputAdelante);
        //Mover el vehiculo hacia los lados.
        transform.Rotate(Vector3.up, VelocidadGiro * InputHorizontal * Time.deltaTime);
    }
}
