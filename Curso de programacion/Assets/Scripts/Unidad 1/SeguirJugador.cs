using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirJugador : MonoBehaviour
{
    public GameObject Jugador;
    private Vector3 offset = new Vector3(0, 5, -12);

    void Start()
    {
        
    }

    void LateUpdate()
    {
        transform.position = Jugador.transform.position + offset;
    }
}
