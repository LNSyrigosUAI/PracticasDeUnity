﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class DestroyOutOfBoundsX : MonoBehaviour
{
    private float leftLimit = -24;
    private float bottomLimit = -1;

    // Update is called once per frame
    void Update()
    {
        // Destruye perros si la posición x es menor que el límite izquierdo.
        if (transform.position.x < leftLimit)
        {
            Destroy(gameObject);
        }
        // Destruye pelotas si la posición y es menor que el límite inferior
        else if (transform.position.y < bottomLimit)
        {
            Destroy(gameObject);
        }
    }
}
