﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerC2 : MonoBehaviour
{
    public GameObject dogPrefab;
    private float GeneracionDelay = 1.5f; // Esta variable define cuánto tiempo debe esperar el jugador antes de poder generar un nuevo perro.
    private float GeneracionDuracion = 0.0f; // Se utiliza para rastrear el tiempo transcurrido desde la última generación del perro.

    void Update()
    {
        // Time.deltaTime: Esta función de Unity devuelve el tiempo transcurrido desde el último cuadro, lo que permite acumular el tiempo correctamente.
        GeneracionDuracion += Time.deltaTime;

        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space) && GeneracionDuracion > GeneracionDelay) // Además de verificar si el jugador presiona la barra espaciadora, también se comprueba si ha pasado el tiempo suficiente. 
        {
            Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);

            GeneracionDuracion = 0.0f;
        }
    }
}
