using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float HorizontalInput;
    public float Velocidad = 10.0f;
    public float RangoX = 10.0f;

    public GameObject PrefabProyectil;

    void Start()
    {
        
    }

    void Update()
    {
        if(transform.position.x < -RangoX)
        {
            transform.position = new Vector3(-RangoX, transform.position.y, transform.position.z);
        }

        if (transform.position.x > RangoX)
        {
            transform.position = new Vector3(RangoX, transform.position.y, transform.position.z);
        }

        HorizontalInput = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * HorizontalInput * Time.deltaTime * Velocidad);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //Lanzar proyectiles desde el jugador.
            Instantiate(PrefabProyectil, transform.position, PrefabProyectil.transform.rotation);
        }
    }
}
