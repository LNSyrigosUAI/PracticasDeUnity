using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSpawn : MonoBehaviour
{
    public GameObject[] PrefabsAnimales;
    private float PosicionX = 12;
    private float PosicionZ = 20;
    private float EmpezarDelay = 2;
    private float EmpezarIntervalo = 1.5f;

    void Start()
    {
        InvokeRepeating("GeneradorRandomAnimales", EmpezarDelay, EmpezarIntervalo);
    }

    void Update()
    {

    }

    void GeneradorRandomAnimales()
    {
        int AnimalIndex = Random.Range(0, PrefabsAnimales.Length);
        Vector3 PosicionSpawn = new Vector3(Random.Range(-PosicionX, PosicionX), 0, PosicionZ);
        Instantiate(PrefabsAnimales[AnimalIndex], PosicionSpawn, PrefabsAnimales[AnimalIndex].transform.rotation);
    }
}
