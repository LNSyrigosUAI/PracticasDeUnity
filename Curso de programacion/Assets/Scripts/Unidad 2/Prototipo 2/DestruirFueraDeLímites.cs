using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirFueraDeLímites : MonoBehaviour
{
    private float ParteSuperior = 30.0f;
    private float ParteInferior = -10.0f;
    void Start()
    {

    }

    void Update()
    {
        if (transform.position.z > ParteSuperior)
        {
            Destroy(gameObject);
        }
        else if (transform.position.z < ParteInferior)
        {
            Debug.Log("PERDISTE");
            Destroy(gameObject);
        }
    }
}
