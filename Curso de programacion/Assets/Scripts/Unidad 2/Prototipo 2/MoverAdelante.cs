using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverAdelante : MonoBehaviour
{
    public float Velocidad = 40.0f;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * Velocidad); 
    }
}
