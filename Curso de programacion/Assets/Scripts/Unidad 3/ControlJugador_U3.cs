using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador_U3 : MonoBehaviour
{
    private Rigidbody JugadorRb;
    public float FuerzaSalto = 10;
    public float ModificadorGravedad;
    public bool EstaEnElSuelo = true;

    void Start()
    {
        JugadorRb = GetComponent<Rigidbody>();
        Physics.gravity *= ModificadorGravedad;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnElSuelo)
        {
            JugadorRb.AddForce(Vector3.up * FuerzaSalto, ForceMode.Impulse);
            EstaEnElSuelo = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        EstaEnElSuelo = true;
    }
}
