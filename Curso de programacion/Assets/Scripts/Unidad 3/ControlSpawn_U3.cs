using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSpawn_U3 : MonoBehaviour
{
    public GameObject ObstaculoPrefab;
    private Vector3 SpawnPos = new Vector3(25, 0, 0);
    private float EmpezarDelay = 2;
    private float TasaRepeticion = 2;

    void Start()
    {
        InvokeRepeating("GenerarObstaculo", EmpezarDelay, TasaRepeticion);
    }

    void Update()
    {
        
    }

    void GenerarObstaculo()
    {
        Instantiate(ObstaculoPrefab, SpawnPos, ObstaculoPrefab.transform.rotation);
    }
}
