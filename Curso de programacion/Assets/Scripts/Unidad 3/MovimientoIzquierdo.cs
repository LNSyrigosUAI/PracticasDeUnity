using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoIzquierdo : MonoBehaviour
{
    private float Velocidad = 30;

    void Start()
    {
        
    }

    void Update()
    {
        transform.Translate(Vector3.left *Time.deltaTime * Velocidad);
    }
}
